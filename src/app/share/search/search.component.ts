import { Component, OnInit, EventEmitter, Input, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hn-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  @Output() onSearch: EventEmitter<Object[]> = new EventEmitter;
  @Input() data: Object[];
  @Input() searchCriteria: Array<string>;
  searchForm: FormGroup;
  private subscription: Subscription;

	constructor(private fb: FormBuilder) {
    this.searchForm = this.fb.group({
      searchValue:  new FormControl(''),
    });
  }

  ngOnInit() {
    this.checkConditions();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  checkConditions() {
    this.subscription = this.searchForm.controls["searchValue"].valueChanges.pipe(
      debounceTime(500)
    ).subscribe(data => this.search(data)); 
  }

  search(string: string) {
    let searchResult;
    if (string.length > 1) {
      searchResult = this.data.filter(obj => {
        for (const i in this.searchCriteria) {
          if (obj[this.searchCriteria[i]].toString().toLowerCase().includes(string.toLowerCase())) {
            return obj; 
          } 
        }
      });
    } else if (string.length <= 1){
        searchResult = this.data;
    }
    this.onSearch.emit(searchResult);
  }
}