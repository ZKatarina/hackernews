import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/service/login.service';
import { User } from 'src/app/models/user';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hn-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User;
  hasUser: boolean = true;
  loginForm: FormGroup;
  createAccountForm: FormGroup;
  private subscription: Subscription;
  
  constructor(private loginService: LoginService, private fb: FormBuilder, private router: Router) {
    this.loginForm = this.fb.group({
      username: new FormControl(null, [Validators.required]),
	    password: new FormControl(null, [Validators.required])
    });
    this.createAccountForm = this.fb.group({
      username: new FormControl(null, [Validators.required]),
	    password: new FormControl(null, [Validators.required])
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
  onSubmit() {
    const userId = this.loginForm.get('username').value;
    this.subscription = this.loginService.getUser(userId).subscribe(user => {
      if(user) {
        this.user = user;
        this.router.navigate(['/stories']);
      } else {
        this.hasUser = false;
      }
      this.loginService.login(this.user);
    },
    (error) => {
      this.hasUser = false;
    }
    );
  }

  createAccount() {
    this.loginService.addUser(this.createAccountForm.value).subscribe(user => console.log(user));
  }
}
