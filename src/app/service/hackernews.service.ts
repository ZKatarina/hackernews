import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Story } from '../models/story';

@Injectable({
  providedIn: 'root'
})
export class HackernewsService {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'https://hacker-news.firebaseio.com/v0';
   }

  getListStoryId(): Observable<number[]> {
    return this.http.get<number[]>(`${this.baseUrl}/topstories.json`);
  }

  getStory(storyId: number): Observable<Story> {
    return this.http.get<Story>(`${this.baseUrl}/item/${storyId}.json`);
  }

  getComment(commentID: number): Observable<Comment> {
    return this.http.get<Comment>(`${this.baseUrl}/item/${commentID}.json`);
  }

  addComment(newComment: Comment): Observable<Comment> {
    return this.http.post<Comment>(`${this.baseUrl}/item`, newComment);
  }
  
}