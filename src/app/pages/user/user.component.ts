import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/service/login.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hn-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  private subscription: Subscription;
  id: string;
  user: User;
  constructor(private route: ActivatedRoute, private userService: LoginService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.subscription = this.userService.getUser(this.id).subscribe(user => this.user = user);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
