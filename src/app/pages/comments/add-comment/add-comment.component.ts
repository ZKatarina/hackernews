import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Story } from 'src/app/models/story';
import { LoginService } from 'src/app/service/login.service';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { HackernewsService } from 'src/app/service/hackernews.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'hn-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.scss']
})
export class AddCommentComponent implements OnInit, OnDestroy {
  commentForm: FormGroup;
  @Input() story: Story;
  private subscription: Subscription[] = [];
  currentUser: User;
  commentId: number;

  constructor(private fb: FormBuilder, private userService: LoginService, 
    private nhService: HackernewsService, private route: ActivatedRoute) {
    this.commentForm = this.fb.group({
      by: [],
      parent: [],
      text: new FormControl(null, [Validators.required]),
      time: [],
      type: "comment"
    });
   }

  ngOnInit(): void {
    this.commentId = this.route.snapshot.params.commentId;
    this.subscription.push(this.userService.currentUser.subscribe(async data => {
      this.currentUser = await data;
    }));
  }

  ngOnDestroy() {
    this.subscription.forEach(s => s.unsubscribe());
  }
   
  onSubmit() {
    if (this.currentUser){
      this.commentForm.get('time').setValue(Math.round((new Date()).getTime() / 1000));
      this.commentForm.get('by').setValue(this.currentUser.id);
      this.commentForm.get('parent').setValue(this.story.id);
      this.subscription.push(this.nhService.addComment(this.commentForm.value)
        .subscribe(comment => console.log(comment)));
    }
  }

}
