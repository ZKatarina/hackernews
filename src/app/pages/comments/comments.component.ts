import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HackernewsService } from 'src/app/service/hackernews.service';
import { Story } from 'src/app/models/story';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'hn-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit, OnDestroy {
  id: number;
  story: Story;
  private subscription: Subscription[] = [];
  numComments: number = 4;
  showButton: boolean = true;
  currentUser: User;

  constructor(private route: ActivatedRoute, private hnService: HackernewsService, 
    private router: Router, private userService: LoginService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.subscription.push(this.hnService.getStory(this.id).subscribe(story => 
        { this.story = story; },
      (error) => {
        this.router.navigate(['/error/not-found']);
    }));
    this.subscription.push(this.userService.currentUser.subscribe(async data => {
      this.currentUser = await data;
    }));
  }

  ngOnDestroy() {
    this.subscription.forEach(s => s.unsubscribe());
  }

  showMoreComments() {
    this.numComments = this.numComments + 2;
    if (this.story.descendants > this.numComments) {
      this.showButton = false;
    }
  }

}
