import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { HackernewsService } from 'src/app/service/hackernews.service';
import { LoginService } from 'src/app/service/login.service';
import { ActivatedRoute } from '@angular/router';
import { Story } from 'src/app/models/story';

@Component({
  selector: 'hn-comment-reply',
  templateUrl: './comment-reply.component.html',
  styleUrls: ['./comment-reply.component.scss']
})
export class CommentReplyComponent implements OnInit {
  commentForm: FormGroup;
  private subscription: Subscription[] = [];
  currentUser: User;
  commentId: number;
  storyId: number;
  parentComment: any;
  story: Story;

  constructor(private fb: FormBuilder, private userService: LoginService, 
    private hnService: HackernewsService, private route: ActivatedRoute) {
    this.commentForm = this.fb.group({
      by: [],
      parent: [],
      text: new FormControl(null, [Validators.required]),
      time: [],
      type: "comment"
    });
   }

  ngOnInit(): void {
    this.commentId = this.route.snapshot.params.commentId;
    this.storyId = this.route.snapshot.params.id;
    this.subscription.push(this.userService.currentUser.subscribe(async data => {
      this.currentUser = await data;
    }));
    this.subscription.push(this.hnService.getComment(this.commentId).subscribe( comment => 
      this.parentComment = comment ));
    this.subscription.push(this.hnService.getStory(this.storyId)
      .subscribe(story => this.story = story));
  }

  ngOnDestroy() {
    this.subscription.forEach(s => s.unsubscribe());
  }

  onSubmit() {
    if (this.currentUser){
      this.commentForm.get('time').setValue(Math.round((new Date()).getTime() / 1000));
      this.commentForm.get('by').setValue(this.currentUser.id);
      this.commentForm.get('parent').setValue(this.parentComment["id"]);
      this.subscription.push(this.hnService.addComment(this.commentForm.value)
        .subscribe(comment => console.log(comment)));
    }
  }
}
