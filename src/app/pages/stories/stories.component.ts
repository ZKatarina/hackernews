import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HackernewsService } from 'src/app/service/hackernews.service';
import { Story } from 'src/app/models/story';
import { Subscription, Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'hn-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  private subscription: Subscription[] = [];
  stories: Story[] = new Array;
  displayedColumns: string[] = ['index', 'story'];
  dataSource: any = [];
  searchCriteria: string[] = ["title", "by", "score"];
  currentUser: User;
  
  constructor(private hnService: HackernewsService, private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
    this.subscription.push(this.hnService.getListStoryId().subscribe(idList => 
      idList.forEach((id, i) => this.hnService.getStory(id).subscribe(story => {
        this.stories[i] = story;
        this.dataSource = new MatTableDataSource(this.stories);
        this.dataSource.paginator = this.paginator;
      },
      (error) => {
        this.router.navigate(['/error/not-found']);
      }
    ))));
    this.subscription.push(this.loginService.currentUser.subscribe(async data => {
      this.currentUser = await data;
    }));
  }

  ngOnDestroy() {
    this.subscription.forEach(s => s.unsubscribe());
  }

  hideStory(id: number) {
    this.stories = this.stories.filter(story => story.id !== id);
  }

  search(storiesOfSearch: Story[]){
    this.dataSource = new MatTableDataSource(storiesOfSearch);
    this.dataSource.paginator = this.paginator;
  }

  vote(id: number) {
    this.stories.forEach(story => {
      if (story.id === id) {
        story.vote = true;
      }
    });
  }

  unvote(id: number) {
    this.stories.forEach(story => {
      if (story.id === id) {
        story.vote = false;
      }
    });
  }

}