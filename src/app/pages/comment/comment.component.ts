import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { HackernewsService } from 'src/app/service/hackernews.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/service/login.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'hn-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit, OnDestroy {
  @Input() commentId: number;
  comment: any;
  show: boolean = true;
  private subscription: Subscription[] = [];
  hasVote: boolean = false;
  currentUser: User;
  
  constructor( private hnService: HackernewsService, private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
    this.subscription.push(this.hnService.getComment(this.commentId).subscribe( comment => 
      { this.comment = comment; },
      (error) => {
        this.router.navigate(['/error/not-found']);
      }
    ));
    this.subscription.push(this.loginService.currentUser.subscribe(async data => {
      this.currentUser = await data;
    }));
  }

  toggleCommentBody() {
    this.show = !this.show;
  }

  ngOnDestroy() {
    this.subscription.forEach(s => s.unsubscribe());
  }

  vote(){
    this.hasVote = !this.hasVote;
  }

}
