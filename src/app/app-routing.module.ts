import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoriesComponent } from './pages/stories/stories.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { WrongRouteComponent } from './core/wrong-route/wrong-route.component';
import { LoginComponent } from './share/login/login.component';
import { UserComponent } from './pages/user/user.component';
import { CommentReplyComponent } from './pages/comments/comment-reply/comment-reply.component';


const routes: Routes = [
  {path: 'stories', component: StoriesComponent},
  {path: 'stories/:id/comments', component: CommentsComponent},
  {path: 'stories/:id/comments/:commentId', component: CommentReplyComponent},
  {path: 'login', component: LoginComponent},
  {path: 'user/:id', component: UserComponent},
  {path: "error/not-found", component: WrongRouteComponent},
  {path:'', redirectTo: '/stories', pathMatch: 'full'},
  {path: "**", component: WrongRouteComponent, pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
