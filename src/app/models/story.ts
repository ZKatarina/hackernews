import { Moment } from 'moment'

export interface Story {
    by: string;
    descendants: number;
    id: number;
    kids: number[];
    score: number;
    time: Moment;
    title: string;
    type: string;
    url: string;
    vote?: boolean;
}