import { Moment } from 'moment';

export interface User {
    about? : string;
    created? : Moment;
    delay? : number;
    id : string;
    karma? : number;
    submitted? : []
}