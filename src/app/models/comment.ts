import { Moment } from 'moment';

export interface Comment {
    by: string;
    id: number;
    kids: [];
    parent: number;
    text: string;
    time: Moment;
    type: string;
} 