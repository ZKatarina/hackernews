import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from 'src/app/service/login.service';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hn-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  currentUser: User;
  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
    this.subscription = this.loginService.currentUser.subscribe(async data => {
      this.currentUser = await data;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logout() {
    this.loginService.logout();
  }

}
