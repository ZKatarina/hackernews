import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'ngx-moment';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { StoriesComponent } from './pages/stories/stories.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShortDomainPipe } from './pipes/short-domain.pipe';
import { WrongRouteComponent } from './core/wrong-route/wrong-route.component';
import { CommentComponent } from './pages/comment/comment.component';
import { SearchComponent } from './share/search/search.component';
import { LoginComponent } from './share/login/login.component';
import { UserComponent } from './pages/user/user.component';
import { AddCommentComponent } from './pages/comments/add-comment/add-comment.component';
import { CommentReplyComponent } from './pages/comments/comment-reply/comment-reply.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StoriesComponent,
    CommentsComponent,
    ShortDomainPipe,
    WrongRouteComponent,
    CommentComponent,
    SearchComponent,
    LoginComponent,
    UserComponent,
    AddCommentComponent,
    CommentReplyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MomentModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
